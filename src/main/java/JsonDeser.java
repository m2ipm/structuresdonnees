/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import org.universite.m2fi.refproduits.Produits;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author akriks
 */
public class JsonDeser {
    
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Produits listProduits = objectMapper.readValue(new File("src/main/resources/MesProduits.json"), Produits.class);
        
        listProduits.getProduit().forEach(prod -> {
             System.out.println(prod.getId()+"--"+prod.getNom()+"--"+prod.getCategorie());
         });
    }
    
}
