/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.universite.m2fi.refproduits.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;

/**
 *
 * @author akriks
 */
public class JaxbSerializing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        System.out.println("On va créer un  bean et le serialiser en XML ");
        

        // Nous allons créer 2 instances de ProduitType et les ajouter à la liste des produits
        Produits listProduits=new Produits();
        
        // 1ier produit
        ProduitType produit=new ProduitType();
        produit.setId("qp995742fr");
        produit.setNom("Pantalon jean SEREZIO");
        produit.setDescription("Pantanon tout naze");
        PrixType prix=new PrixType();
        prix.setMontant(BigDecimal.valueOf(123.34));
        prix.setRemise(15);
        produit.setPrix(prix);
        produit.setCategorie(CategorieType.VETEMENT);
        
        //2ième produit
        ProduitType produit2=new ProduitType();
        produit2.setId("ba7358fr");
        produit2.setNom("Raquette TX5");
        produit2.setDescription("Raquette de tennis en porcelaine");
        PrixType prix2=new PrixType();
        prix2.setMontant(BigDecimal.valueOf(415.30));
        prix2.setRemise(10);
        produit2.setPrix(prix2);
        produit2.setCategorie(CategorieType.SPORT);  
        
        // ajout à la liste
        listProduits.getProduit().add(produit);
        listProduits.getProduit().add(produit2);
        
        
        JAXBContext contextObj = JAXBContext.newInstance(Produits.class);

        Marshaller marshallerObj = contextObj.createMarshaller();
        marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        
        marshallerObj.marshal(listProduits, new FileOutputStream("./src/main/resources/MesProduits.xml"));

    }

}
