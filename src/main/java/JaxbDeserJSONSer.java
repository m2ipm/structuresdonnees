/*
 * Nous allons déserialiser (unmarshall) le document XML précédement pour avoir des instances d'objet Java
 * puis nous allons les utiliser pour les serialiser en JSON
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import org.universite.m2fi.refproduits.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author akriks
 */
public class JaxbDeserJSONSer {
   

    public static void main(String[] args) throws JAXBException, IOException {  
   

   
        File file = new File("./src/main/resources/MesProduits.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Produits.class);  
   
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
        Produits listProduits= (Produits) jaxbUnmarshaller.unmarshal(file);  
        
         // On vérifie que les beans java sont bien créés
         for (ProduitType prod : listProduits.getProduit()) {
             System.out.println(prod.getId()+"--"+prod.getNom()+"--"+prod.getCategorie());
         }
         
        /* ou en programmation fonctionnelle 
        listProduits.getProduit().forEach(prod -> {
             System.out.println(prod.getId()+"--"+prod.getNom()+"--"+prod.getCategorie());
         }); */
   
        // On serialize maintenant en JSON
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File("./src/main/resources/MesProduits.json"), listProduits);

    }  
}  
    

